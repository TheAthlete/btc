CREATE USER btc_usr WITH PASSWORD 'qwerty';

CREATE DATABASE btc
  WITH  OWNER       = btc_usr
        TEMPLATE    = template0
        ENCODING    = 'UTF8'
        LC_COLLATE  = 'ru_RU.UTF-8'
        LC_CTYPE    = 'ru_RU.UTF-8';

GRANT ALL PRIVILEGES ON DATABASE btc TO btc_usr;
