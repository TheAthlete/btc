# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_06_07_093827) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "candle", primary_key: ["symbol", "timestamp"], force: :cascade do |t|
    t.text "symbol", null: false
    t.decimal "open", comment: "Open price"
    t.decimal "close", comment: "Close price"
    t.decimal "min", comment: "Min price"
    t.decimal "max", comment: "Max price"
    t.decimal "volume", comment: "Volume in base currency"
    t.decimal "volumequote", comment: "Volume in quote currency"
    t.datetime "timestamp", null: false, comment: "Last update or refresh ticker timestamp"
  end

  create_table "currency", id: :text, comment: "Currency identifier. In the future, the description will simply use the currency", force: :cascade do |t|
    t.text "fullname", comment: "Currency full name"
    t.boolean "crypto", comment: "Is currency belongs to blockchain (false for ICO and fiat, like EUR)"
    t.boolean "payinenabled", comment: "Is allowed for deposit (false for ICO)"
    t.boolean "payinpaymentid", comment: "Is required to provide additional information other than the address for deposit"
    t.boolean "payoutenabled", comment: "Is allowed for withdraw (false for ICO)"
    t.boolean "payoutispaymentid", comment: "Is allowed to provide additional information for withdraw"
    t.boolean "transferenabled", comment: "Is allowed to transfer between trading and account (may be disabled on maintain)"
    t.boolean "delisted", comment: "True if currency delisted (stopped deposit and trading)"
    t.decimal "payinconfirmations", precision: 12, scale: 6, comment: "Blocks confirmations count for deposit"
    t.decimal "payoutfee", precision: 20, scale: 10, comment: "Default withdraw fee"
  end

  create_table "symbol", id: :text, comment: "Symbol identifier. In the future, the description will simply use the symbol", force: :cascade do |t|
    t.text "basecurrency"
    t.text "quotecurrency"
    t.decimal "quantityincrement", precision: 16, scale: 10
    t.decimal "ticksize", precision: 14, scale: 12
    t.decimal "takeliquidityrate", precision: 8, scale: 6, comment: "Default fee rate"
    t.decimal "provideliquidityrate", precision: 12, scale: 8, comment: "Default fee rate for market making trades"
    t.text "feecurrency"
  end

  create_table "ticker", primary_key: "symbol", id: :text, force: :cascade do |t|
    t.datetime "timestamp", comment: "Last update or refresh ticker timestamp"
    t.decimal "ask", precision: 20, scale: 12, comment: "Best ask price"
    t.decimal "bid", precision: 20, scale: 12, comment: "Best bid price"
    t.decimal "last", precision: 20, scale: 12, comment: "Last trade price"
    t.decimal "open", precision: 20, scale: 12, comment: "Last trade price 24 hours ago"
    t.decimal "low", precision: 20, scale: 12, comment: "Lowest trade price within 24 hours"
    t.decimal "high", precision: 20, scale: 12, comment: "Highest trade price within 24 hours"
    t.decimal "volume", precision: 20, scale: 8, comment: "Total trading amount within 24 hours in base currency"
    t.decimal "volumequote", precision: 22, scale: 12, comment: "Total trading amount within 24 hours in quote currency"
  end

  create_table "trade", primary_key: ["symbol", "id"], force: :cascade do |t|
    t.integer "id", null: false, comment: "trade id"
    t.text "symbol", null: false
    t.text "side", comment: "trade side sell or buy"
    t.decimal "price", precision: 20, scale: 10, comment: "trade price"
    t.decimal "quantity", precision: 20, scale: 10, comment: "trade quantity"
    t.datetime "timestamp", comment: "trade timestamp"
  end

  add_foreign_key "candle", "symbol", column: "symbol"
  add_foreign_key "symbol", "currency", column: "basecurrency"
  add_foreign_key "symbol", "currency", column: "feecurrency"
  add_foreign_key "symbol", "currency", column: "quotecurrency"
  add_foreign_key "ticker", "symbol", column: "symbol"
  add_foreign_key "trade", "symbol", column: "symbol"
end
