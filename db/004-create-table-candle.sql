\c btc;

create table candle (
  symbol text primary key references symbol(id),
  open numeric(14,10),
  close numeric(14,10),
  min numeric(14,10),
  max numeric(14,10),
  volume numeric(14,10),
  volumeQuote numeric(24,18),
  timestamp timestamp with time zone
);

GRANT ALL PRIVILEGES ON candle TO btc_usr;

comment on column candle.open        is 'Open price';
comment on column candle.close       is 'Close price';
comment on column candle.min         is 'Min price';
comment on column candle.max         is 'Max price';
comment on column candle.volume      is 'Volume in base currency';
comment on column candle.volumeQuote is 'Volume in quote currency';







