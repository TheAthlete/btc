class ChangeCandlePrimaryKey < ActiveRecord::Migration[5.2]
  def up
    execute "alter table candle drop constraint candle_pkey"
    execute "alter table candle add constraint candle_symbol_timestamp_pkey primary key (symbol, timestamp)"
  end

  def down
    execute "alter table candle drop constraint candle_symbol_timestamp_pkey"
    execute "alter table candle add constraint candle_pkey primary key (symbol)"
  end
end
