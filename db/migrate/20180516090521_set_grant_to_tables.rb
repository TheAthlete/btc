class SetGrantToTables < ActiveRecord::Migration[5.2]
  def up
    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"

    create_table "currency", id: false, force: :cascade do |t|
      t.text "id",                    comment: "Currency identifier. In the future, the description will simply use the currency"
      t.text "fullname",              comment: "Currency full name"
      t.boolean "crypto",             comment: "Is currency belongs to blockchain (false for ICO and fiat, like EUR)"
      t.boolean "payinenabled",       comment: "Is allowed for deposit (false for ICO)"
      t.boolean "payinpaymentid",     comment: "Is required to provide additional information other than the address for deposit"
      t.boolean "payoutenabled",      comment: "Is allowed for withdraw (false for ICO)"
      t.boolean "payoutispaymentid",  comment: "Is allowed to provide additional information for withdraw"
      t.boolean "transferenabled",    comment: "Is allowed to transfer between trading and account (may be disabled on maintain)"
      t.boolean "delisted",           comment: "True if currency delisted (stopped deposit and trading)"
      t.decimal "payinconfirmations", precision: 12, scale: 6, comment: "Blocks confirmations count for deposit"
      t.decimal "payoutfee",          precision: 20, scale: 10, comment: "Default withdraw fee"
    end

    execute "alter table currency add primary key (id)"

    create_table "symbol", id: false, force: :cascade do |t|
      t.text    "id",                                comment: "Symbol identifier. In the future, the description will simply use the symbol"
      t.text    "basecurrency"
      t.text    "quotecurrency"
      t.decimal "quantityincrement",    precision: 16, scale: 10
      t.decimal "ticksize",             precision: 14, scale: 12
      t.decimal "takeliquidityrate",    precision: 8, scale: 6,  comment: "Default fee rate"
      t.decimal "provideliquidityrate", precision: 12, scale: 8, comment: "Default fee rate for market making trades"
      t.text    "feecurrency"
    end

    execute "alter table symbol add primary key (id)"

    add_foreign_key :symbol, :currency, column: :basecurrency, primary_key: "id"
    add_foreign_key :symbol, :currency, column: :feecurrency, primary_key: "id"
    add_foreign_key :symbol, :currency, column: :quotecurrency, primary_key: "id"

    create_table "ticker", id: false, force: :cascade do |t|
      t.text    "symbol"
      t.column  "timestamp",   "timestamp with time zone", comment: "Last update or refresh ticker timestamp"
      t.decimal "ask",         precision: 20, scale: 12,   comment: "Best ask price"
      t.decimal "bid",         precision: 20, scale: 12,   comment: "Best bid price"
      t.decimal "last",        precision: 20, scale: 12,   comment: "Last trade price"
      t.decimal "open",        precision: 20, scale: 12,   comment: "Last trade price 24 hours ago"
      t.decimal "low",         precision: 20, scale: 12,   comment: "Lowest trade price within 24 hours"
      t.decimal "high",        precision: 20, scale: 12,   comment: "Highest trade price within 24 hours"
      t.decimal "volume",      precision: 20, scale: 8,   comment: "Total trading amount within 24 hours in base currency"
      t.decimal "volumequote", precision: 22, scale: 12,    comment: "Total trading amount within 24 hours in quote currency"
    end

    execute "alter table ticker add primary key (symbol)"
    add_foreign_key :ticker, :symbol, column: :symbol, primary_key: "id"

    create_table "trade", primary_key: ["symbol", "id"], force: :cascade do |t|
      t.integer "id",                                    comment: "trade id"
      t.text    "symbol"
      t.text    "side",                                  comment: "trade side sell or buy"
      t.decimal "price",    precision: 20, scale: 10,      comment: "trade price"
      t.decimal "quantity", precision: 20, scale: 10,      comment: "trade quantity"
      t.column  "timestamp", "timestamp with time zone", comment: "trade timestamp"
    end

    add_foreign_key :trade, :symbol, column: :symbol, primary_key: "id"

    grant_statements = <<-SQL
      GRANT ALL PRIVILEGES ON currency TO btc_usr;
      GRANT ALL PRIVILEGES ON symbol TO btc_usr;
      GRANT ALL PRIVILEGES ON ticker TO btc_usr;
      GRANT ALL PRIVILEGES ON trade TO btc_usr;
    SQL

    ActiveRecord::Base.connection.execute grant_statements
  end

  def down
    revoke_statements = <<-SQL
      REVOKE ALL PRIVILEGES ON currency FROM btc_usr;
      REVOKE ALL PRIVILEGES ON symbol FROM btc_usr;
      REVOKE ALL PRIVILEGES ON ticker FROM btc_usr;
      REVOKE ALL PRIVILEGES ON trade FROM btc_usr;
    SQL

    ActiveRecord::Base.connection.execute revoke_statements

    drop_table "currency"
    drop_table "symbol"
    drop_table "ticker"
    drop_table "trade"
  end
end
