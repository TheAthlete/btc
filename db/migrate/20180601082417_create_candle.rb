class CreateCandle < ActiveRecord::Migration[5.2]
  def up
    create_table :candle, id: false do |t|
      t.text    "symbol"
      t.decimal "open",        comment: "Open price"
      t.decimal "close",       comment: "Close price"
      t.decimal "min",         comment: "Min price"
      t.decimal "max",         comment: "Max price"
      t.decimal "volume",      comment: "Volume in base currency"
      t.decimal "volumequote", comment: "Volume in quote currency"
      t.column  "timestamp", "timestamp with time zone", comment: "Last update or refresh ticker timestamp"
    end

      # t.decimal "open",        precision: 24, scale: 18, comment: "Open price"
      # t.decimal "close",       precision: 24, scale: 18, comment: "Close price"
      # t.decimal "min",         precision: 24, scale: 18, comment: "Min price"
      # t.decimal "max",         precision: 24, scale: 18, comment: "Max price"
      # t.decimal "volume",      precision: 24, scale: 18, comment: "Volume in base currency"
      # t.decimal "volumequote", precision: 24, scale: 18, comment: "Volume in quote currency"

    execute "alter table candle add primary key (symbol)"
    add_foreign_key :candle, :symbol, column: :symbol, primary_key: "id"

    grant_statements = <<-SQL
      GRANT ALL PRIVILEGES ON candle TO btc_usr;
    SQL

    ActiveRecord::Base.connection.execute grant_statements

  end

  def down
    revoke_statements = <<-SQL
      REVOKE ALL PRIVILEGES ON candle FROM btc_usr;
    SQL

    ActiveRecord::Base.connection.execute revoke_statements

    drop_table "candle"
  end
end
