CREATE USER btc_usr WITH PASSWORD 'qwerty';

CREATE DATABASE btc
  WITH  OWNER       = btc_usr
        TEMPLATE    = template0
        ENCODING    = 'UTF8'
        LC_COLLATE  = 'ru_RU.UTF-8'
        LC_CTYPE    = 'ru_RU.UTF-8';

GRANT ALL PRIVILEGES ON DATABASE btc TO btc_usr;

\c btc;

create table currency (
  id text primary key,
  fullName text,
  crypto boolean,
  payinEnabled boolean,
  payinPaymentId boolean,
  payoutEnabled boolean,
  payoutIsPaymentId boolean,
  transferEnabled boolean,
  delisted boolean,
  payinConfirmations numeric(12, 6),
  payoutFee numeric(20, 10)
);

comment on column currency.id is 'Currency identifier. In the future, the description will simply use the currency';
comment on column currency.fullName is 'Currency full name';
comment on column currency.crypto is 'Is currency belongs to blockchain (false for ICO and fiat, like EUR)';
comment on column currency.payinEnabled is 'Is allowed for deposit (false for ICO)';
comment on column currency.payinPaymentId is 'Is required to provide additional information other than the address for deposit';
comment on column currency.payinConfirmations is 'Blocks confirmations count for deposit';
comment on column currency.payoutEnabled is 'Is allowed for withdraw (false for ICO)';
comment on column currency.payoutIsPaymentId is 'Is allowed to provide additional information for withdraw';
comment on column currency.transferEnabled is 'Is allowed to transfer between trading and account (may be disabled on maintain)';
comment on column currency.delisted is 'True if currency delisted (stopped deposit and trading)';
comment on column currency.payoutFee is 'Default withdraw fee';

GRANT ALL PRIVILEGES ON currency TO btc_usr;

create table symbol (
  id text primary key,
  baseCurrency text references currency (id),
  quoteCurrency text references currency (id),
  feeCurrency text references currency (id),
  quantityIncrement numeric(16, 10),
  tickSize numeric(14, 12),
  takeLiquidityRate numeric(8, 6),
  provideLiquidityRate numeric(12, 8)
);

comment on column symbol.id is 'Symbol identifier. In the future, the description will simply use the symbol';
comment on column symbol.takeLiquidityRate is 'Default fee rate';
comment on column symbol.provideLiquidityRate is 'Default fee rate for market making trades';

GRANT ALL PRIVILEGES ON symbol TO btc_usr;

create table ticker (
  symbol text primary key references symbol(id),
  ask numeric(20, 12),
  bid numeric(20, 12),
  last numeric(20, 12),
  open numeric(20, 12),
  low numeric(20, 12),
  high numeric(20, 12),
  volume numeric(20, 8),
  volumeQuote numeric(22, 12),
  timestamp timestamp with time zone
);

comment on column ticker.ask is 'Best ask price';
comment on column ticker.bid is 'Best bid price';
comment on column ticker.last is 'Last trade price';
comment on column ticker.open is 'Last trade price 24 hours ago';
comment on column ticker.low is 'Lowest trade price within 24 hours';
comment on column ticker.high is 'Highest trade price within 24 hours';
comment on column ticker.volume is 'Total trading amount within 24 hours in base currency';
comment on column ticker.volumeQuote is 'Total trading amount within 24 hours in quote currency';
comment on column ticker.timestamp is 'Last update or refresh ticker timestamp';

GRANT ALL PRIVILEGES ON ticker TO btc_usr;

create table trade (
  symbol text references symbol(id),
  id integer,
  side text,
  price numeric(20, 10),
  quantity numeric(20, 10),
  timestamp timestamp with time zone,
  primary key (symbol, id)
);

comment on column trade.id is 'trade id';
comment on column trade.price is 'trade price';
comment on column trade.quantity is 'trade quantity';
comment on column trade.side is 'trade side sell or buy';
comment on column trade.timestamp is 'trade timestamp';

GRANT ALL PRIVILEGES ON trade TO btc_usr;
