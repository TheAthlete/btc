class AuthController < ApplicationController
  def index
    @symbols = Symbols.all
  end

  def symbol
    @symbol = Symbols.find(params[:id].upcase)
    @ticker = Tickers.find(params[:id].upcase)
    @trades = Trades.where(symbol: params[:id].upcase)
  end

  def candle
    @candles = Candles.where(symbol: params[:symbol].upcase)
  end
end
