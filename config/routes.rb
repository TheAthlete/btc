Rails.application.routes.draw do
  root 'auth#index'
  get '/symbol/:id', to: 'auth#symbol', as: 'symbol'
  get '/candle/:symbol', to: 'auth#candle', as: 'candle'
end
