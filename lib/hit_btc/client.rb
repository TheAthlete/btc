require "http"
require 'oj'
require 'date'
require 'text-table'

module HitBtc
  class Client

    def initialize

      # TODO:
      # 1. raise "" -> raise ArguamentError, ""
      # 2. Заменить HTTP.rb на socketry/{nio4r or Async::HTTP} или em-http-request

      # dispatch table для генерации урлов
      @url = {
        currencies: -> (param = "") { "https://api.hitbtc.com/api/2/public/currency/#{param}" },
        symbols:    -> (param = "") { "https://api.hitbtc.com/api/2/public/symbol/#{param}" },
        tickers:    -> (param = "") { "https://api.hitbtc.com/api/2/public/ticker/#{param}" },
        trades:     -> (param, query = {}) { 
          raise "Error! Query parameter 'sort' support only 'ASC' or 'DESC'"   if query[:sort] && !%w{asc desc}.include?(query[:sort].downcase)
          raise "Error! Query parameter 'by' support only 'id' or 'timestamp'" if query[:by] && !%w{id timestamp}.include?(query[:by].downcase)

          if query[:limit]
            raise "Error! Query parameter 'limit' max is 1000" if query[:limit] > 1000
            raise "Error! Query parameter 'limit' must be positive" if query[:limit] < 0
          end

          if query[:offset]
            raise "Error! Query parameter 'offset' must be positive" if query[:offset] < 0
          end

          # дата передается в урл в UTC таймзоне в формате
          # $ ruby -rdate -e 'dates = %w{2015-08-25 2015-08-25T13:00:00 2015-08-25T13:59:22.770Z}; dates.each{ |d| pp DateTime.parse(d).strftime("%FT%T.%L") }'
          #  "2015-08-25T00:00:00.000"
          #  "2015-08-25T13:00:00.000"
          #  "2015-08-25T13:59:22.770"
          query[:from] = DateTime.parse(query[:from]).strftime("%FT%T.%L") if query[:from]
          query[:till] = DateTime.parse(query[:till]).strftime("%FT%T.%L") if query[:till]

          query_string = ''
          query_string = '?' + query.map{|k, v| "#{k}=#{v}"}.join('&') unless query.empty?

          "https://api.hitbtc.com/api/2/public/trades/#{param}#{query_string}"
        },
        candles:    ->(param, query = {}) {
          if query[:limit]
            raise "Error! Query parameter 'limit' max is 1000" if query[:limit] > 1000
            raise "Error! Query parameter 'limit' must be positive" if query[:limit] < 0
          end
          if query[:period] && !%w{M1 M3 M5 M15 M30 H1 H4 D1 D7 1M}.include?(query[:period].upcase!)
            raise "Error! Query parameter 'period' support only 'M1' (one minute), 'M3', 'M5', 'M15', 'M30', 'H1', 'H4', 'D1', 'D7' or '1M' (one month)" 
          end

          query_string = ''
          query_string = '?' + query.map{|k, v| "#{k}=#{v}"}.join('&') unless query.empty?

          "https://api.hitbtc.com/api/2/public/candles/#{param}#{query_string}"
        }
      }

    end

    # Примеры использования
    # client.get(:currencies)
    # client.get(:currencies, {currency: "BTC"})
    # client.get(:symbols)
    # client.get(:symbols, {symbol: "JOTBTC"})
    # client.get(:tickers)
    # client.get(:tickers, {symbol: "JOTBTC"})
    # client.get(:trades, {symbol: "JOTBTC"})
    # client.get(:candles, {symbol: "JOTBTC"})
    def get(market_type, params = {})
      param = nil
      if [:currencies, :symbols, :tickers, :trades, :candles].include? market_type
        param = market_type == :currencies ? :currency : :symbol
        params[param] ||= ""
      else
        raise "Error! Unknown market type: #{market_type.to_s}"
      end

      raise 'Error! Symbol is missing. Please set symbol' if [:candles, :trades].include?(market_type) && params[:symbol] == ""

      url = @url[market_type].(params[param])
      response = HTTP.get(url)
      if response.code == 200
        return Oj.load(response.to_s)
      else
        raise "[get_#{market_type}] url: " + url + " " + response.inspect + "\n" + response.to_s
      end
    end

    def print_table(head, rows)
      table = Text::Table.new
      table.head = head

      case rows
      when Array
        rows.each do |row|
          table.rows << row.values_at(*head)
        end
      when Hash
        table.rows << rows.values_at(*head)
      end
      puts table.to_s
    end

  end
end
