# Сайт для отслеживания котировок криптовалют

Используемые технологии:

* Ruby 2.5.1
* Ruby on Rails 5.2.0

Установка и настройка проекта:

    $ git clone https://TheAthlete@bitbucket.org/TheAthlete/btc.git
    $ cd btc
    $ bundle
    $ sudo -u postgres psql < db/001-create-btc-database.sql
    $ bin/rails db:migrate
    $ bin/rails s
