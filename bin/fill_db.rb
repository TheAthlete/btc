#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path(File.dirname(__FILE__) + '/../lib')

require 'hit_btc/client'

require 'pg'
require 'getoptlong'
require 'ruby-progressbar'
require 'logger'

opts = GetoptLong.new(
  [ '--help',       '-h', GetoptLong::NO_ARGUMENT ],
  [ '--currencies', '-c', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--symbols',    '-s', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--tickers',    '-t', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--trades',     '-r', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--candles',     '-n', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--progressbar', '-p', GetoptLong::OPTIONAL_ARGUMENT ]
)

currencies, symbols, tickers, trades, candles, progressbar = nil, nil, nil, nil, nil, nil

logger = Logger.new(File.expand_path(File.dirname(__FILE__) + '/../log/fill_db.log'))
logger.level = Logger::WARN

opts.each do |opt, arg|
  case opt
    when '--help'
      puts <<-EOF
./fill_db.rb [OPTIONS]

-h, --help:
   show help

--currencies [currency], -c [currency]:
    actual list of available currencies, tokens, ICO etc.

--symbols [symbol], -s [symbol]:
    actual list of currency symbols (currency pairs) traded on HitBTC exchange.

--tickers [symbol], -t [symbol]:
    ticker information

--trades symbol, -r symbol:

--candles symbol, -n symbol:

--progressbar, -p
  use only with --trades or --candles
      EOF
    when '--currencies'
      currencies = arg
    when '--symbols'
      symbols = arg
    when '--tickers'
      tickers = arg
    when '--trades'
      trades = arg
    when '--candles'
      candles = arg
    when '--progressbar'
      progressbar = arg
  end
end

client = HitBtc::Client.new
conn = PG.connect(host: '127.0.0.1', port: 5432, dbname: 'btc', user: 'btc_usr', password: 'qwerty')

if currencies
  conn.prepare(
    'currency_row_sth', 
    'insert into currency (id, fullname, crypto, payinenabled, payinpaymentid, payinconfirmations, payoutenabled, payoutispaymentid, transferenabled, delisted, payoutfee) 
     values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
       on conflict (id) 
       do update set id=excluded.id, fullname=excluded.fullname, crypto=excluded.crypto, payinenabled=excluded.payinenabled, 
                    payinpaymentid=excluded.payinpaymentid, payinconfirmations=excluded.payinconfirmations, payoutenabled=excluded.payoutenabled, 
                    payoutispaymentid=excluded.payoutispaymentid, transferenabled=excluded.transferenabled, delisted=excluded.delisted, payoutfee=excluded.payoutfee'
  )

  begin

    conn.transaction do |connect|
      client.get(:currencies, {currency: currencies}).each do |currency| 
        connect.exec_prepared('currency_row_sth', currency.values_at(*%w{id fullName crypto payinEnabled payinPaymentId payinConfirmations payoutEnabled payoutIsPaymentId transferEnabled delisted payoutFee}))
      end
    end

  rescue PG::Error => e
    
    logger.error '[currencies] ' + e.message 

  rescue => ex

    logger.error '[currencies] ' + ex.message

  end
end

if symbols
  conn.prepare(
    'symbol_row_sth', 
     'insert into symbol 
        (id, basecurrency, quotecurrency, quantityincrement, ticksize, takeliquidityrate, provideliquidityrate, feecurrency) 
      values 
        ($1, $2, $3, $4, $5, $6, $7, $8)
      on conflict (id) 
      do update set id=excluded.id, basecurrency=excluded.basecurrency, quotecurrency=excluded.quotecurrency, 
                    quantityincrement=excluded.quantityincrement, ticksize=excluded.ticksize, takeliquidityrate=excluded.takeliquidityrate, 
                    provideliquidityrate=excluded.provideliquidityrate, feecurrency=excluded.feecurrency'
  )

  begin
    conn.transaction do |connect|
      client.get(:symbols, {symbol: symbols}).each do |symbol| 
        connect.exec_prepared('symbol_row_sth', symbol.values_at(*%w{id baseCurrency quoteCurrency quantityIncrement tickSize takeLiquidityRate provideLiquidityRate feeCurrency}))
      end
    end
  rescue PG::Error => e
    logger.error '[symbols] ' + e.message 
  rescue => ex
    logger.error '[symbols] ' + ex.message
  end
end

if tickers
  conn.prepare(
    'ticker_row_sth', 
    'insert into ticker 
       (ask, bid, last, open, low, high, volume, volumeQuote, timestamp, symbol) 
     values 
       ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
     on conflict (symbol) 
     do update set ask=excluded.ask, bid=excluded.bid, last=excluded.last, open=excluded.open, low=excluded.low, high=excluded.high, 
                    volume=excluded.volume, volumeQuote=excluded.volumeQuote, timestamp=excluded.timestamp, symbol=excluded.symbol'
              )

  begin

    # TODO: Обработать ошибку при загрузке symbol INKETH:
    # {
#   "ask": "0.000372",
#   "bid": "0.000027",
#   "last": null,
#   "open": null,
#   "low": "",
#   "high": "",
#   "volume": "0",
#   "volumeQuote": "0.00000",
#   "timestamp": "2018-06-05T13:58:49.282Z",
#   "symbol": "INKETH"
# }
    # для low и high задаются пустые строки
    conn.transaction do |connect|
      client.get(:tickers, {symbol: tickers}).each do |ticker| 
        connect.exec_prepared('ticker_row_sth', ticker.values_at(*%w{ask bid last open low high volume volumeQuote timestamp symbol}))
      end
    end

  rescue PG::Error => e

    logger.error '[tickers] ' + e.message 

  rescue => ex

    logger.error '[tickers] ' + ex.message

  end
end

if trades
  conn.prepare(
    'trade_row_sth', 
    'insert into trade 
       (symbol, id, side, price, quantity, timestamp)
     values 
       ($1, $2, $3, $4, $5, $6)
    on conflict (symbol, id) 
    do update set symbol=excluded.symbol, id=excluded.id, side=excluded.side, price=excluded.price, quantity=excluded.quantity, timestamp=excluded.timestamp'
  )

  begin

    symbol_ids = client.get(:symbols).map { |symbol| symbol["id"] }

    pbr = ProgressBar.create(:title => "Trades", :total => symbol_ids.length) if progressbar

    conn.transaction do |connect|
      symbol_ids.each do |symbol_id|
        pbr.increment if progressbar

        client.get(:trades, {symbol: symbol_id}).each do |trade| 
          conn.exec_prepared('trade_row_sth', [symbol_id, *trade.values_at(*%w{id side price quantity timestamp})])
        end
      end
    end

  rescue PG::Error => e

    logger.error '[trades] ' + e.message 

  rescue => ex

    logger.error '[trades] ' + ex.message

  end
end

if candles

  conn.prepare(
    'candle_row_sth', 
    'insert into candle 
       (symbol, open, close, min, max, volume, volumeQuote, timestamp)
     values 
       ($1, $2, $3, $4, $5, $6, $7, $8)
    on conflict (symbol, timestamp) 
    do update set symbol=excluded.symbol, open=excluded.open, close=excluded.close, min=excluded.min, max=excluded.max, volume=excluded.volume, 
                  volumeQuote=excluded.volumeQuote, timestamp=excluded.timestamp'
  )

  begin

    symbol_ids = client.get(:symbols).map { |symbol| symbol["id"] }

    pbr = ProgressBar.create(:title => "Candles", :total => symbol_ids.length) if progressbar

    conn.transaction do |connect|
      symbol_ids.each do |symbol_id|
        pbr.increment if progressbar
        # TODO: добавить прокси

        client.get(:candles, {symbol: symbol_id}).each do |candle| 
          conn.exec_prepared('candle_row_sth', [symbol_id, *candle.values_at(*%w{open close min max volume volumeQuote timestamp})])
        end

      end

    end

  rescue PG::Error => e

    logger.error '[candles] PG::Error ' + e.message 

  rescue => ex

    logger.error '[candles] ' + ex.message

  end
end
